//
// Created by Matus PC on 30. 12. 2020.
//

#include "../Header/snake.h"
#include <stdio.h>
#include "stdlib.h"
#include <pthread.h>
#include <sys/socket.h>
#include <sstream>

// producent
void *snake::vytvorJedlo(void *args) {
    Data *data = (Data *) args;
    bool zastav = false;
    while (!zastav) {
        pthread_mutex_lock(data->mutex);
        while (data->prepinacVlaken >= POCET_JEDLA) {
            pthread_cond_wait(data->zjedzJedlo, data->mutex);
        }
        if (data->prepinacVlaken == 0) {
            for (int i = data->prepinacVlaken; i < POCET_JEDLA; ++i) {
                int pravdepodobnostJedla = rand() % 10;
                if (pravdepodobnostJedla == 9) {
                    data->jedlo[i] = data->zleJedlo;
                } else {
                    data->jedlo[i] = data->dobreJedlo;
                }

                data->poziciaJedlaX[i] = rand() % (data->sirka) + 1;
                data->poziciaJedlaY[i] = rand() % (data->vyska);

                for (int j = 0; j < i; ++j) {
                    if (data->poziciaJedlaX[j] == data->poziciaJedlaX[i] &&
                        data->poziciaJedlaY[j] == data->poziciaJedlaY[i]) {

                        data->poziciaJedlaX[i] = rand() % (data->sirka) + 1;
                        data->poziciaJedlaY[i] = rand() % (data->vyska);
                        j = 0;
                    }
                }
                data->prepinacVlaken++;
            }
        }
        pthread_cond_signal(data->vytvorJedlo);
        pthread_mutex_unlock(data->mutex);

        if (data->poradieHada == 5) {
            zastav = true;
        }
    }

}

// konzument
void *snake::spracujJedlo(void *args) {
    Data *data = (Data *) args;
    while (!data->gameTerminated) {
        pthread_mutex_lock(data->mutex);
        while (data->prepinacVlaken <= 0) {
            cout << "Cakam na jedlo" << endl;
            sendToClient(data->clientSocket, "Cakam na jedlo\n");
            pthread_cond_wait(data->vytvorJedlo, data->mutex);
        }
        vykresliSa(data);
        for (int i = 0; i < data->pocClankov; ++i) {
            if (data->teloX[i] == data->poziciaHadaX && data->teloY[i] == data->poziciaHadaY) {
                if (data->poradieHada == 5) {
                    data->gameTerminated = true;

                } else {
                    data->predosla2Y = -1;
                    data->predosla2X = -1;
                    data->predoslaX = -1;
                    data->predoslaY = -1;
                    data->pocClankov = 0;
                    data->poradieHada++;
                    data->prepinacVlaken = 0;
                }
            }
        }

        if (data->poziciaHadaX >= data->sirka + 1 || data->poziciaHadaX <= 0 || data->poziciaHadaY >= data->vyska ||
            data->poziciaHadaY < 0) {
            data->poziciaHadaX = rand() % (data->sirka) + 1;
            data->poziciaHadaY = rand() % (data->vyska);
            if (data->poradieHada == 5) {
                data->gameTerminated = true;
            } else {
                data->predosla2Y = -1;
                data->predosla2X = -1;
                data->predoslaX = -1;
                data->predoslaY = -1;
                data->pocClankov = 0;
                data->poradieHada++;
                data->prepinacVlaken = 0;
            }
        } else {
            for (int i = 0; i < POCET_JEDLA; ++i) {
                if (data->poziciaHadaX == data->poziciaJedlaX[i] && data->poziciaHadaY == data->poziciaJedlaY[i]) {
                    if (data->jedlo[i] == data->zleJedlo) {
                        data->score[data->poradieHada] -= 2;
                        data->pocClankov++;
                    } else {
                        data->score[data->poradieHada] += 5;
                    }
                    data->poziciaJedlaX[i] = -1;
                    data->poziciaJedlaY[i] = -1;
                    data->prepinacVlaken--;
                    data->pocClankov++;
                }
            }
        }
        pthread_cond_signal(data->zjedzJedlo);
        pthread_mutex_unlock(data->mutex);
    }
}

snake::snake(int vyska, int sirka, int serverVyber, int clientVyber, int clientSocket) {
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex, NULL);

    pthread_cond_t vytvorJedlo;
    pthread_cond_init(&vytvorJedlo, NULL);

    pthread_cond_t zjedzJedlo;
    pthread_cond_init(&zjedzJedlo, NULL);

    data = {{0}, {0}, 'G', 'B', {0}, &mutex, &vytvorJedlo, &zjedzJedlo, sirka / 5, vyska / 5, false, sirka, vyska, {0},
            0, 0, {'S', 'M', 'F', 'T', 'O', 'X'}, clientSocket, 0, {0}, {0}, 0, 0, 0, 0, 0};

    pthread_t vlaknoKonzument;
    pthread_t vlaknoProducent;

    pthread_create(&vlaknoProducent, NULL, &snake::vytvorJedlo, &data);
    pthread_create(&vlaknoKonzument, NULL, &snake::spracujJedlo, &data);

    pthread_join(vlaknoProducent, NULL);

    pthread_join(vlaknoKonzument, NULL);

    string message;
    for (int i = 0; i < 6; ++i) {
        stringstream messagestream;
        messagestream << "Score " << (i + 1) << " hada: " << data.score[i] << endl;
        message = messagestream.str();
        sendToClient(data.clientSocket, message);
        cout << message;
    }

    if (data.score[serverVyber] == data.score[clientVyber]) {
        cout << "Remiza " << endl;
        sendToClient(data.clientSocket, "Remiza \n");
    }
    if (data.score[serverVyber] > data.score[clientVyber]) {
        cout << "Vyhrava hrac 1 (server)" << endl;
        sendToClient(data.clientSocket, "Vyhrava hrac 1 (server)\n");
    }
    if (data.score[serverVyber] < data.score[clientVyber]) {
        cout << "Vyhrava hrac 2 (client)" << endl;
        sendToClient(data.clientSocket, "Vyhrava hrac 2 (client)\n");
    }

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&vytvorJedlo);
    pthread_cond_destroy(&zjedzJedlo);
}

void *snake::vykresliSa(void *args) {
    string message;
    Data *data = (Data *) args;

    if (!data->gameTerminated) {
        for (int i = 0; i < data->sirka + 2; ++i) {
            cout << "#";
            sendToClient(data->clientSocket, "#");
        }
        cout << endl;
        sendToClient(data->clientSocket, "\n");
        for (int i = 0; i < data->vyska; ++i) {
            for (int j = 0; j < data->sirka + 1; ++j) {
                bool prazdnyZnak = false;
                if (j == 0) {
                    cout << "#";
                    sendToClient(data->clientSocket, "#");
                    prazdnyZnak = true;
                } else if (data->poziciaHadaY == i && data->poziciaHadaX == j) {
                    message = data->hlavaHada[data->poradieHada];
                    cout << message;
                    sendToClient(data->clientSocket, message);
                    prazdnyZnak = true;
                } else {
                    for (int k = 0; k < POCET_JEDLA; ++k) {
                        if (data->poziciaJedlaY[k] == i && data->poziciaJedlaX[k] == j) {
                            message = data->jedlo[k];
                            cout << message;
                            sendToClient(data->clientSocket, message);
                            prazdnyZnak = true;
                        }
                    }
                }
                for (int k = 0; k < data->pocClankov; ++k) {
                    if (data->teloX[k] == j && data->teloY[k] == i) {
                        cout << "o";
                        sendToClient(data->clientSocket, "o");
                        prazdnyZnak = true;
                    }
                }
                if (!prazdnyZnak) {
                    cout << " ";
                    sendToClient(data->clientSocket, " ");
                }
            }
            cout << "#";
            sendToClient(data->clientSocket, "#");

            sendToClient(data->clientSocket, "\n");
            cout << endl;
        }
        for (int i = 0; i < data->sirka + 2; ++i) {
            cout << "#";
            sendToClient(data->clientSocket, "#");
        }
        stringstream messagestream;
        messagestream << "\nScore: " << data->score[data->poradieHada] << endl;
        message = messagestream.str();
        cout << message;

        sendToClient(data->clientSocket, message);

        data->predoslaX = data->teloX[0];
        data->predoslaY = data->teloY[0];
        data->teloX[0] = data->poziciaHadaX;
        data->teloY[0] = data->poziciaHadaY;
        for (int i = 1; i < data->pocClankov; ++i) {
            data->predosla2X = data->teloX[i];
            data->predosla2Y = data->teloY[i];
            data->teloX[i] = data->predoslaX;
            data->teloY[i] = data->predoslaY;
            data->predoslaX = data->predosla2X;
            data->predoslaY = data->predosla2Y;
        }
        stringstream newmessagestream;
        int pohyb;
        do {
            pohyb = rand() % 4 + 1;
            if (pohyb == 1 && data->pohybHada != 2) {
                data->poziciaHadaY++;
                newmessagestream << "Idem Dole som na poziciach X:" << data->poziciaHadaX << " Y: "
                                 << data->poziciaHadaY << endl;
                data->pohybHada = pohyb;
            } else if (pohyb == 2 && data->pohybHada != 1) {
                data->poziciaHadaY--;
                newmessagestream << "Idem Hore som na poziciach X:" << data->poziciaHadaX << " Y: "
                                 << data->poziciaHadaY << endl;
                data->pohybHada = pohyb;
            } else if (pohyb == 3 && data->pohybHada != 4) {
                data->poziciaHadaX++;
                newmessagestream << "Idem Vpravo som na poziciach X:" << data->poziciaHadaX << " Y: "
                                 << data->poziciaHadaY << endl;
                data->pohybHada = pohyb;
            } else if (pohyb == 4 && data->pohybHada != 3) {
                data->poziciaHadaX--;
                newmessagestream << "Idem Vlavo som na poziciach X: " << data->poziciaHadaX << " Y: "
                                 << data->poziciaHadaY << endl;
                data->pohybHada = pohyb;
            } else {
                pohyb = 0;
            }
        } while (pohyb != data->pohybHada);
        message = newmessagestream.str();
        sendToClient(data->clientSocket, message);
        cout << message;
    }
}

void *snake::sendToClient(int clientSocket, string message) {
    send(clientSocket, message.c_str(), message.size() + 1, 0);
    char buf[4096];
    while (true) {
        int bytesReceived = recv(clientSocket, buf, 4096, 0);
        if (bytesReceived == -1) {
            cerr << "Error in recv(). Quitting" << endl;
            return 0;
        }

        if (bytesReceived == 0) {
            cout << "Client disconnected " << endl;
            return 0;
        }

        if (bytesReceived > 0) {
            break;
        }
    }
}












