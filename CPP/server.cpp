//
// Created by Matus PC on 30. 12. 2020.
//

#include "../Header/server.h"
#include "../Header/snake.h"
#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string>
#include <string.h>

using namespace std;

int server::serverBJ() {
    // Create a socket
    int listening = socket(AF_INET, SOCK_STREAM, 0);
    if (listening == -1) {
        cerr << "Can't create a socket! Quitting" << endl;
        return -1;
    }

    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(28025);
    inet_pton(AF_INET, "0.0.0.0", &hint.sin_addr);

    bind(listening, (sockaddr *) &hint, sizeof(hint));

    listen(listening, SOMAXCONN);

    sockaddr_in client;
    socklen_t clientSize = sizeof(client);

    int clientSocket = accept(listening, (sockaddr *) &client, &clientSize);

    char host[NI_MAXHOST];
    char service[NI_MAXSERV];

    memset(host, 0, NI_MAXHOST);
    memset(service, 0, NI_MAXSERV);

    if (getnameinfo((sockaddr *) &client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0) {
        cout << host << " connected on port " << service << endl;
    } else {
        inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
        cout << host << " connected on port " << ntohs(client.sin_port) << endl;
    }
    close(listening);

    char buf[4096];
    bool vybralHadaClient = false;
    bool vybralHadaServer = false;
    int clientVyber;
    int serverVyber;
    while (true) {
        cout << "Vyber si hada, zadanim cisla od 1 do 6"<< endl;
        memset(buf, 0, 4096);
        int bytesReceived = recv(clientSocket, buf, 4096, 0);
        if (bytesReceived == -1) {
            cerr << "Error in recv(). Quitting" << endl;
            break;
        }

        if (bytesReceived == 0) {
            cout << "Client disconnected " << endl;
            return 0;
        }

        cout << "CLIENT>" << string(buf, 0, bytesReceived) << endl;

        if (!vybralHadaClient) {
            clientVyber = stoi(string(buf, 0, bytesReceived));
            if (clientVyber > 0 && clientVyber < 7) {
                vybralHadaClient = true;
            } else {
                string odpoved = "Zly vyber hada";
                send(clientSocket, odpoved.c_str(), odpoved.size() + 1, 0);
            }
        }
        if (!vybralHadaServer) {
            string userInput;
            getline(cin, userInput);
            serverVyber = stoi(userInput);
            if (serverVyber > 0 && serverVyber < 7) {
                vybralHadaServer = true;
                send(clientSocket, userInput.c_str(), userInput.size() + 1, 0);
            } else {
                cout << "Zly vyber hada" << endl;
            }
        }

        if (vybralHadaServer && vybralHadaClient) {
            snake(10, 10, serverVyber - 1, clientVyber - 1, clientSocket);
            break;
        }

    }

    cout << "Ending comunication with client" << endl;
    close(clientSocket);

    return 0;
}
