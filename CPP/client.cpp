//
// Created by ferow on 29. 12. 2020.
//

#include "../Header/client.h"
#include <iostream>
//#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>

using namespace std;

int client::clientCBJ() {

    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        return 1;
    }

    int port = 28025;
    string ipAddress = "0.0.0.0";

    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(port);
    inet_pton(AF_INET, ipAddress.c_str(), &hint.sin_addr);

    int connectRes = connect(sock, (sockaddr *) &hint, sizeof(hint));
    if (connectRes == -1) {
        return 1;
    }

    char buf[4096];
    string userInput;
    bool vybralHada = false;
    while (!vybralHada) {
        cout << "Vyber si hada, zadanim cisla od 1 do 6"<< endl;
        getline(cin, userInput);
        int clientVyber = stoi(userInput);
        if (clientVyber > 0 && clientVyber < 7) {
            vybralHada = true;
        }
        int sendRes = send(sock, userInput.c_str(), userInput.size() + 1, 0);
        if (sendRes == -1) {
            cout << "Could not send to server! Whoops!\r\n";
            continue;
        }
        memset(buf, 0, 4096);
        int bytesReceived = recv(sock, buf, 4096, 0);
        if (bytesReceived == 0) {
            cout << "Server ended communication" << endl;
            return 0;
        } else if (bytesReceived == -1) {
            cout << "There was an error getting response from server\r\n";
            break;
        } else {
            cout << "SERVER vybral > " << string(buf, 0, bytesReceived) << "\r\n";
        }
    }

    string response = " ";
    do {
        memset(buf, 0, 4096);
        int bytesReceived = recv(sock, buf, 4096, 0);
        if (bytesReceived == 0) {
            cout << "Server ended communication" << endl;
            return 0;
        } else if (bytesReceived == -1) {
            cout << "There was an error getting response from server\r\n";
            break;
        } else {
            cout << string(buf, 0, bytesReceived);
            send(sock, response.c_str(), response.size() + 1, 0);
        }
    } while (true);

    cout << "Client ended communication with server" << endl;
    close(sock);
    return 0;
}

