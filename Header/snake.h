//
// Created by Matus PC on 30. 12. 2020.
//
#include <iostream>
#include <string>

using namespace std;
#ifndef AWESOME_SNAKE_SNAKE_H
#define AWESOME_SNAKE_SNAKE_H


class snake {
private:
#define POCET_JEDLA 3
    typedef struct Data {
        int poziciaJedlaX[POCET_JEDLA];
        int poziciaJedlaY[POCET_JEDLA];

        char dobreJedlo;
        char zleJedlo;
        char jedlo[POCET_JEDLA];

        pthread_mutex_t *mutex;
        pthread_cond_t *vytvorJedlo;
        pthread_cond_t *zjedzJedlo;

        int poziciaHadaX;
        int poziciaHadaY;

        bool gameTerminated;

        int sirka;
        int vyska;

        int score[6];
        int poradieHada;
        int prepinacVlaken;
        char hlavaHada[6];
        int clientSocket;
        int pohybHada;

        int teloX[50];
        int teloY[50];
        int predoslaX;
        int predoslaY;
        int predosla2X;
        int predosla2Y;
        int pocClankov;
    } Data;
    Data data;

    static void *vykresliSa(void *args);

    static void *sendToClient(int clientSocket, string message);

    static void *spracujJedlo(void *args);

    static void *vytvorJedlo(void *args);

public:
    snake(int vyska, int sirka, int vyberServer, int vyberClient, int clientSocket);
};


#endif //AWESOME_SNAKE_SNAKE_H
